from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse

from .models import ToDoItem


def index(request):
    # return HttpResponse("Hello from the views.py file")
    todoitem_list = ToDoItem.objects.all()
    output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
    return HttpResponse(output)


def todoitem(request, todoitem_id):
    response = "You are viewing the details of %s"
    return HttpResponse(response % todoitem_id)