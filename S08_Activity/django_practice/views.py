from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse


# def index(request):
#     return HttpResponse("Hello from the views.py file")
from .models import GroceryItem
from django.template import loader

def index(request):
    items = GroceryItem.objects.all()
    template = loader.get_template('django_practice/index.html')
    context = {
        'items': items,
    }
    return HttpResponse(template.render(context, request))